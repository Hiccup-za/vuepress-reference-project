# Changelog

All notable changes to this project will be documented in this file.  
The format is based on [Keep a Changelog][1], and this project adheres to [Semantic Versioning][2].

## [1.2.2] - 2020-07-26

### Updated

- Updated the repository `README.md` with dependency information

## [1.2.1] - 2020-07-26

### Updated

- Updated Stylesheet with code examples
- Updated markdownlint rules

## [1.2.0] - 2020-07-26

### Added

- Added PrettyCheckboxVue dependency and required files

### Updated

- Updated Stylesheet with Checkbox types
- Updated markdownlint rules

## [1.1.0] - 2020-07-26

### Added

- Added the Yuu theme to enable dark mode options

### Removed

- Removed Details Custom Container due to incompatibility with the Yuu theme
- Removed the box shadow from the colorBadge component

### Updated

- Updated the text colour of the colorBadge component to black for dark mode compatibility
- Updated the repository `README.md`

## [1.0.0] - 2020-07-26

### Changed

- Updated the repository `README.md` to contain information, requirements and how to deploy locally

[1]: https://keepachangelog.com/en/1.0.0/
[2]: https://semver.org/spec/v2.0.0.html
