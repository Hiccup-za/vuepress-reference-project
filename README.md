# VuePress Reference Project

[Click here to view the live website.][1]

A [VuePress][2] reference project.
Includes a [stylesheet.][3]

[Click here to view the Changelog.][6]

## Dependencies

The following dependencies are used in this project:

- Theme from [Yuu][7]
- Checkbox component from [PrettyCheckboxVue][8]

## Requirements

- [Yarn][4]
- [Node.js][5] ^=8

## Local Deployment

```sh
# Install Dependencies
yarn install

# Deploy
yarn dev

# Access deployment
"http://localhost:8080/"
```

[1]: https://hiccup-za.gitlab.io/vuepress-reference-project/
[2]: https://hiccup-za.gitlab.io/vuepress-reference-project/stylesheet/
[3]: https://vuepress.vuejs.org/
[4]: https://yarnpkg.com/en/docs/install
[5]: https://nodejs.org/en/download/
[6]: CHANGELOG.md
[7]: https://github.com/danktuary/vuepress-theme-yuu
[8]: https://github.com/hamed-ehtesham/pretty-checkbox-vue
