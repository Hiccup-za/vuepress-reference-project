import Vue from 'vue'
import PrettyCheckbox from 'pretty-checkbox-vue';

export default ({ Vue, options, router, siteData }) => {
  Vue.use(PrettyCheckbox);
}