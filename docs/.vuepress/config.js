module.exports = {
  title: 'VuePress Reference Project',
  base: '/vuepress-reference-project/',
  dest: './public',
  head: [
    ['link', { rel: 'icon', href: 'images/logo.png' }]
  ],
  theme: 'yuu',
  themeConfig: {
    lastUpdated: 'Last Updated',
    nav: [
      { text: 'Stylesheet', link: '/stylesheet/' },
      { text: 'Folder', link: '/folder/' }
    ],
    sidebarDepth: 3,
    sidebar: {
      '/stylesheet/': [
        ''
      ],
      '/folder/': [
        {
          title: 'Folder Title',
          collapsable: false,
          path: '/folder/',
          children: [
            ['file.md', 'File']
          ]
        },
      ]
    }
  }
}