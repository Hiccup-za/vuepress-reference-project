# Stylesheet

## Badges

- <colorbadge text="Blue" type="blue"/>
- <colorbadge text="Green" type="green"/>
- <colorbadge text="Light Green" type="lightGreen"/>
- <colorbadge text="Light Red" type="lightRed"/>
- <colorbadge text="Orange" type="orange"/>
- <colorbadge text="Purple" type="purple"/>
- <colorbadge text="Red" type="red"/>
- <colorbadge text="Yellow" type="yellow"/>

```html
- <colorbadge text="Blue" type="blue"/>
- <colorbadge text="Green" type="green"/>
- <colorbadge text="Light Green" type="lightGreen"/>
- <colorbadge text="Light Red" type="lightRed"/>
- <colorbadge text="Orange" type="orange"/>
- <colorbadge text="Purple" type="purple"/>
- <colorbadge text="Red" type="red"/>
- <colorbadge text="Yellow" type="yellow"/>
```

## Checkboxes

- <p-check name="check" color="success" v-model="check"></p-check>
- <p-radio name="radio" color="info" v-model="radio"></p-radio>
- <p-radio name="radio" color="info" v-model="radio"></p-radio>

```shell
- <p-check name="check" color="success" v-model="check"></p-check>
- <p-radio name="radio" color="info" v-model="radio"></p-radio>
- <p-radio name="radio" color="info" v-model="radio"></p-radio>
```

## Custom Containers

::: tip Tip
Text goes here.
:::

```markdown
::: tip Tip
Text goes here.
:::
```

::: warning Warning
Text goes here.
:::

```markdown
::: warning Warning
Text goes here.
:::
```

::: danger Danger
Text goes here.
:::

```markdown
::: danger Danger
Text goes here.
:::
```

## Emojis

- ✅
